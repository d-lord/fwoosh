\documentclass[12pt]{article}
%\usepackage{caption}
\usepackage[top=1.5in, bottom=1.5in, left=1in, right=1in]{geometry} % page margins
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{compat=1.8}
\usepackage{fancyhdr}
\pagestyle{fancy}

\setlength{\parindent}{0pt} % LaTeX, why do you do otherwise?

\fancyfoot[L]{David Lord}
\fancyfoot[C]{\thepage}
\fancyfoot[R]{43039070}

\begin{document}

\section{Introduction and definitions}
This report discusses the results of testing ``fwoosh'', a parallel program to make fractal zoom videos. See previous reports for more general information about it.
\subsection{Cluster}
All tests were run on Savanna; I wanted to test on both Savanna and Barrine.
I encountered ``invalid MPI world'' errors on Barrine for long enough to impede testing, and wrote all the tests with Savanna's PBS format instead.

\subsection{Logs}
All processes in the job log their activity to stdout. PBS combines stdout into one file, \verb=$JOBNAME.o$JOBID=. Because of synchronisation and flushing questions, the file is not immediately updated or necessarily in the correct order: groups of messages from one process tend to appear at once. So it's not a reliable guide to order.

The log does record chunks allocated, events for each frame start and completion (including CPU time used), and communications. Because of PBS' stdout-combining feature, it is simple to start logging a new type of useful data.

I've written scripts to parse the log which are very dependent on the log's specific format.

\subsection{Verification and input/output files}
Verification was described in more detail in the parallel report. In short: output images are downloaded after a run, used in a video, and the video is briefly inspected for jumps/artefacts.
In general, this report doesn't include output, because there's nothing to say beyond ``confirmed to be correct''. Timing is the focus of the report: so I produce the same video with different parameters and time it.
There are only a couple of unique videos produced (50-100 and 1-400). \footnote{Also, submitting video files through subversion is a difficult question.}
Input is very straightforward, too: there are only a few parameters (resolution, iterations, resources, start and end depth) to be configured. There is no ``input'' beyond that; the fractal comes from within mathematics(!).
See section \ref{sec:scientific-experiment} for more discussion of input/output.

\subsection{Resource notation}
There are two types of resource available: MPI processes and cores per process. The resource usage of a job is denoted as such:
1x8 denotes 1 MPI process and 8 cores.
2x4 denotes 2 MPI processes and 4 cores per process.
In reality, there will be an additional 1x1 on every job: the master process requires 1x1, but will not benefit from any more. This notation usually refers to the slave process/es, because they are the component that changes.\\
In reality, the master process (to coordinate the slaves) will add 1x1 to every job. It will not benefit from any more resources and is a required component of the job. I don't record this in parameters because it is constant.

\subsection{Holdup}
``Holdup'' is a problem I observed during the strong tests; it is when the master rank gives a chunk of work to a slave rank which is much more difficult than the others' jobs. It can become a problem towards the end of a job, when most slaves have finished the slave with the massive allocation is still working on the chunk.
I initially noticed it during the parallel implementation phase, and implemented a new distribution algorithm to fix it -- instead of the master rank allocating all of the work between each slave on startup, it would allocate a percentage of the work and keep the rest for later distribution.
During strong testing, I modified the algorithm to distribute $\frac{1}{10}$ of work (both initial and throughout) instead of $\frac{1}{4}$. This fixed the problem where one rank would hold up the others at the end.

\section{Changes from parallel implementation}
\subsection{clock() and clock\_gettime()}
The submitted parallel implementation used clock() for timing, which is not threadsafe: it reports total CPU time rather than thread CPU time. The following data indicates the problem; it is based on the same job parameters as given below.
\begin{table}[h]
    \begin{tabular}{|l|l|l|}
	\hline
	Reported draw time (s)& 4 threads & 8 threads \\ \hline
	Frame 0   & 45        & 95        \\ \hline
	Frame 49  & 38        & 56        \\ \hline
	Frame 99  & 70        & 142       \\ \hline
	Frame 149 & 80        & 162       \\ \hline
	Frame 199 & 24        & 27        \\ \hline
    \end{tabular}
\end{table}
So the reported time increases with threads. I confirmed with logs that the actual times were the same. Note that at frame 199, there is very little difference: this is because there are very few other threads running while 199 is being drawn. I suspect the increase is just random variation.
I replaced \verb=clock()= with \verb=clock_gettime()= to track time per thread, and the reported frame times are very similar now (that is, within random variation).


\section{Scaling with OpenMP threads}
\subsection{Initial test}
This test examines performance improvements from using multiple threads.
A single process was given a variable thread count and the draw time plotted.
Maximum walltime was set to 1h30.
The following fixed parameters were used. Note that frame count is high and results in large wall time: this is required so that multiple threads have plenty of work each.
Tests were run three times in total, and each result had only a few seconds' variation.
\begin{figure}[h]
    \caption{Parameters for OpenMP scaling test}
    \begin{verbatim}
    export RES=1024
    export ITERS=2000
    export STARTZOOM=50
    export ENDZOOM=100
    export FRAMES=200
    export OUTPUT_DIRECTORY=img
    \end{verbatim}
\end{figure}
This was the initial result:\\

\begin{tikzpicture} % note: INITIAL result (with badness)
    \begin{axis}[
	xlabel=Number of threads, ylabel=Time (s),
	ymin=0,
	xtick=data,
	title=Draw time vs thread count for a single process
    ]
	\addplot coordinates {
	    % how do I format the time data?
	    (1, 3512) % 58:32
	    (2, 3545) % 59:05
	    (3, 1275) % 21:15
	    (4, 1013) % 16:53
	    (5, 867) % 14:27
	    (6, 766) % 12:46
	    (7, 706) % 11:46
	    (8, 630) % 10:30
	};
    \end{axis}
\end{tikzpicture}
\subsection{Analysis}
\paragraph{} This data looks ideal except for the result with 2 threads.
3 threads takes just over $\frac{1}{3}$ of the single-threaded time, 4 takes just over $\frac{1}{4}$, and so on.
So, as a whole, this looks very successful.

\paragraph{} Now we'll discuss number 2, the outlying data point.
The 2-threaded result is concerning. I repeated the tests at the time, and found it was repeatable (with minor variation). I inspected the code, but the OpenMP component is simple and gave no clear reason to suspect it.

\subsection{Retest}
\paragraph{} The key was to repeat the test with a full node reserved. With these modifications, the recalcitrant 2-thread point was found to fit the curve well.
Of course, the rest of the data had to be tested with the full node too. The data was very similar (with the usual minor variation).

\paragraph{} My hypothesis for the change is that while I was running those tests in sequence, PBS found an opening for a two-core job on a busy node, and so my job's performance was degraded.
This didn't cause problems for any of the other jobs: the 1-thread test shares cores much better (I suspect that it can hold its variables in CPU cache and infrequently need to move them to RAM), and the larger tests were run on their own node. The systems administrator for Savanna kindly confirmed this for me.\footnote{A good lesson in keeping the mails that PBS sends, which give this information.}
So, it happened to be the 2-thread test that was impacted, but it could easily have been multiple tests of any size except 8.

\paragraph{} The new results, with better testing, form a curve much closer to the theory.
So the program scales very well with thread count, up to the limit available on Savanna or Barrine (8 cores).

\begin{tikzpicture} % GOOD result (with full reservation)
    \begin{axis}[
	xlabel=Number of threads, ylabel=Time (s),
	ymin=0,
	xtick=data,
	title=Draw time vs thread count for a single process (full node in test)
    ]
	\addplot coordinates {
	    % how do I format the time data?
	    (1, 3512) % 58:32
	    (2, 1926) % 32:06
	    (3, 1275) % 21:15
	    (4, 1013) % 16:53
	    (5, 867) % 14:27
	    (6, 766) % 12:46
	    (7, 706) % 11:46
	    (8, 630) % 10:30
	};
    \end{axis}
\end{tikzpicture}

\newpage
\section{Strong scaling in full parallel (MPI \& OpenMP)}
\paragraph{} Strong scaling is performance for a fixed task with parallelism increased.
So I set a single task - quite large - and tested its scaling with variable resource counts.
Notation reminder: 2x8 represents 2 slave processes with 8 threads each, plus a 1x1 master process.
The following fixed parameters were used:
\begin{verbatim}
export RES=1024
export ITERS=2000
export STARTZOOM=50
export ENDZOOM=100
export FRAMES=400
\end{verbatim}

\begin{tikzpicture}
    \begin{axis}[
	    xlabel=Resources used, ylabel=Time (s),
	    ymin=50, ybar, fill,
	    width=300,
	    symbolic x coords = {1x8, 2x8, 3x8, 4x8, 5x8, 6x8, 7x8, 8x8}, % hacky but works, ish
	    title=Strong scaling: Draw time for variable resources
	]
	\addplot[fill] table[x=resources, y=time]{
	    resources	time
	    1x8		1389
	    2x8		1027
	    3x8		1219
	    4x8		915
	    5x8		1409
	    6x8		1175
	    7x8		1013
	    8x8		885
	};
    \end{axis}
\end{tikzpicture}

\subsection{Analysis}
\label{strong-anal}
\paragraph{} Good strong scaling would look like the OpenMP graph: time would decay exponentially. This graph clearly doesn't.
\begin{itemize}
    \item{} I discovered the MPI distribution issue (distributing too-large chunks and causing holdup) and fixed it, then reran all these tests.
    \item{} Unlike in the thread scaling test, each slave had a full node, so it couldn't be a competition problem.
    \item{} All tests were run with the same binary.
    \item{} I cleared the image output directory between runs.
    \item{} Network activity could be the cause: each process was sending 8 frames to the filesystem every 30 seconds, with up to 8 processes. Using \verb=/scratch= would be good practice regardless, but implementation priorities. I didn't experiment further with this.
    \item{} I considered overlap: the same frame being drawn multiple times by different jobs. It would appear in the output folder that everything is normal, but additional work would be done. However, each frame's draw is logged twice in stdout -- once upon begin, once upon completion. I checked with a Python script that each frame was drawn exactly once.
    \item{} I considered that master be being overloaded. I haven't implemented instrumentation for it, but from my understanding it's doing very little work: \textless 1ms to break off a chunk, and the rest is light MPI communication. Considering that it's unlikely to be the cause, instrumentation is a low priority - I put my focus into more likely leads, and found the MPI distribution issue, for example.
    \item{} I triple-checked the tests to ensure that they do as intended (correct parameters, etc).
    \item{} Tests were run 2-4 times each, but in all cases stayed within a few seconds of their result.
    \item{} Checked that master was giving out sane amounts of work; confirmed in logs. They're big enough to represent significant work, but not so big as to cause big delays -- all the ranks finish within about a minute of one another, which is ideal.
    \item{} Testing with more resources would be preferred, to gather more data, but exceeding 8x8 seems uncivil.
\end{itemize}
\begin{figure}[h]
    \includegraphics{./latex/overall-frame.png}
    \caption{Frame log. A job will usually have hundreds of these entries.}
\end{figure}

After all these checks, I still \emph{thought} there should be good strong scaling, but I had no explanation for why the data didn't support that. See section \ref{possible-mpi-solution} for further discussion.

\newpage
\section{Weak scaling in full parallel (MPI and OpenMP)}
\paragraph{} It is simple to increase the total work done by the job: increase the number of frames drawn. While each zoom level has a different cost (``yellow'' factor and cost of the other pixels), the total work for a fixed set of zoom levels with more frames increases linearly.
The following parameters were used:
\begin{verbatim}
export RES=1024
export ITERS=2000
export STARTZOOM=50
export ENDZOOM=100
export FRAMES=50
\end{verbatim}
Resources are shown as used by the job, but each node was fully reserved, so 1x4 ran with one slave on four cores but used eight cores (four idle).
50 frames @ 1x4, 100@2x4, 200@4x4, 400@8x4.
Checked the tests, and they do as intended.
Ensured that output folders were cleared between runs and none were simultaneous.
Ensured that any nodes used were fully reserved.
\begin{tikzpicture}
    \begin{axis}[
	    xlabel=Resources, ylabel=Time (s),
	    ymin=50, ybar, fill,
	    width=300,
	    symbolic x coords = {1x4, 2x4, 3x4, 4x4, 5x4, 6x4, 7x4, 8x4}, % hacky but works, ish
	    title=Weak scaling: Draw time for variable job size
	]
	\addplot[fill] table[x=resources, y=time]{
	    resources	time
	    1x4		431
	    2x4		368
	    3x4		485
	    4x4		478
	    5x4		891
	    6x4		889
	    7x4		877
	    8x4		885
	}; % hmm, 8x4 is the same as 8x8 from strong scaling test
    \end{axis}
\end{tikzpicture}
\subsection{Analysis}
Ideally, with perfect weak scaling, the graph would be flat.

Realistically, with near-perfect weak scaling, the graph would start low and slope slightly upwards towards the right.

Instead, it's divided into left and right sections, where a major hit to speed distinctly appears between 4x4 and 5x4.

\section{Strong and weak: odd results}
\paragraph{} Looking at both strong and weak scaling, there seems to be a jump in draw time between 4x and 5x resources. 
Why that is, I don't know: they have different testing parameters.
I used the same testing parameters for both, and increased frame count in weak (with increasing resources) but left it constant in strong (with increasing resources). The difficulty of the frames should be constant between both types of test and all permutations within them.
Overlap, as mentioned in \ref{strong-anal}, was considered as an explanation for both but was rejected.
\subsection{Possible solution}
\label{possible-mpi-solution}
\paragraph{} While watching the image resolution test (1x8), I noticed that towards the end of a job (with \textless 10 frames left), the master would only give one frame at a time to the slave, so OpenMP was being factored out -- 7 cores were idle. This is because when I modified the distribution algorithm to prevent hangup, I didn't consider that it would be best to keep as many threads busy as possible. So with $\frac{1}{10}$ being distributed, any number of remaining frames less than $\frac{\text{\$OMP\_NUM\_THREADS}}{10}$ would cause under-utilisation of the resources.
In that case, modifying the distribution algorithm to be aware of thread utilisation might fix the bizarre scaling and bring it in line with what I expect from the theory: good strong and weak scaling.

So the modified algorithm prevented hangup -- where one node reserved work the others should have been doing -- but effectively disabled multithreading gains as the job came closer to finalising.


\newpage
\section{Frame difficulty}
\paragraph{} Since the serial implementation, I've been observing that frame difficulty increases with depth -- that is, as a job goes on, each frame's CPU time increases.
I finally tested it to see how it changes.

A side-effect of the strong and weak testing is that I've confirmed frame difficulty to be independent across MPI processes. So, to speed up this test, I used two nodes (2x8).

The resulting frame times were put through a bash one-liner and plotted.

\begin{figure}
    \caption{Parameters for difficulty test}
    \begin{verbatim}
export RES=1024
export ITERS=2000
export STARTZOOM=1
export ENDZOOM=400
export FRAMES=400
export OUTPUT_DIRECTORY=img
    \end{verbatim}
\end{figure}

\begin{figure}
    \caption{Bash one-liner to produce (frame, time) pairs from a log}
    \begin{verbatim}
    $ grep "completed" fractal-depth-1-400.o26950 | sort -k8 -n | cut -d' ' -f8,10
    \end{verbatim}
\end{figure}

% Zoom from 1 to 400 in maybe 400 frames, and plot frame times -- early frames will be cheap, later frames HARD
\begin{figure}
    \begin{tikzpicture}
	\begin{axis}[
		%xtick=data,
		xlabel=Overall frame \#,
		ylabel=Draw time for frame (s),
		width=300,
		xmin=0,
		title=Increasing frame difficulty
	    ]
	    \addplot +[mark=none] coordinates {

(0, 63.96) (1, 41.87) (2, 12.65) (3, 3.69) (4, 9.07) (5, 27.84) (6, 62.39) (7, 4.10) (8, 4.27) (9, 4.33) (10, 4.10) (11, 4.26) (12, 4.43) (13, 4.62) (14, 5.51) (15, 4.97) (16, 5.88) (17, 5.36) (18, 5.75) (19, 6.50) (20, 6.68) (21, 6.31) (22, 6.28) (23, 6.51) (24, 6.70) (25, 6.89) (26, 7.06) (27, 7.25) (28, 7.46) (29, 7.61) (30, 7.84) (31, 8.01) (32, 8.19) (33, 8.42) (34, 8.59) (35, 8.79) (36, 8.98) (37, 9.13) (38, 9.48) (39, 9.69) (40, 10.19) (41, 10.06) (42, 10.25) (43, 10.45) (44, 10.64) (45, 12.09) (46, 11.54) (47, 11.20) (48, 11.40) (49, 11.58) (50, 11.86) (51, 12.08) (52, 12.24) (53, 12.41) (54, 12.64) (55, 12.82) (56, 13.02) (57, 13.20) (58, 14.40) (59, 14.58) (60, 14.91) (61, 15.32) (62, 15.23) (63, 15.43) (64, 15.88) (65, 15.84) (66, 16.04) (67, 19.53) (68, 16.56) (69, 19.29) (70, 16.85) (71, 17.08) (72, 17.29) (73, 17.48) (74, 20.78) (75, 18.38) (76, 21.55) (77, 18.29) (78, 19.14) (79, 22.22) (80, 18.89) (81, 20.73) (82, 19.65) (83, 20.00) (84, 19.70) (85, 20.68) (86, 20.11) (87, 20.31) (88, 20.51) (89, 20.77) (90, 21.08) (91, 22.67) (92, 21.36) (93, 21.59) (94, 22.09) (95, 22.20) (96, 22.65) (97, 23.29) (98, 24.16) (99, 25.15) (100, 26.52) (101, 26.60) (102, 26.82) (103, 27.15) (104, 27.11) (105, 27.92) (106, 28.36) (107, 28.78) (108, 29.19) (109, 29.63) (110, 30.02) (111, 30.01) (112, 30.85) (113, 30.78) (114, 31.65) (115, 32.07) (116, 32.44) (117, 32.88) (118, 41.46) (119, 41.57) (120, 42.23) (121, 43.41) (122, 43.43) (123, 43.72) (124, 46.57) (125, 47.20) (126, 48.12) (127, 48.78) (128, 46.67) (129, 47.59) (130, 47.43) (131, 48.64) (132, 50.97) (133, 49.54) (134, 50.48) (135, 50.73) (136, 51.04) (137, 51.60) (138, 52.06) (139, 58.31) (140, 53.58) (141, 53.59) (142, 54.41) (143, 55.99) (144, 55.38) (145, 56.41) (146, 57.95) (147, 60.99) (148, 61.61) (149, 68.92) (150, 63.84) (151, 64.34) (152, 68.46) (153, 66.74) (154, 67.55) (155, 69.28) (156, 70.01) (157, 70.67) (158, 70.57) (159, 71.60) (160, 72.63) (161, 73.67) (162, 74.83) (163, 78.51) (164, 76.72) (165, 78.19) (166, 80.49) (167, 79.91) (168, 83.15) (169, 83.73) (170, 84.79) (171, 89.48) (172, 90.07) (173, 94.78) (174, 103.89) (175, 96.23) (176, 100.79) (177, 111.38) (178, 102.40) (179, 106.06) (180, 107.86) (181, 111.04) (182, 113.80) (183, 120.02) (184, 123.90) (185, 127.81) (186, 133.18) (187, 136.70) (188, 142.86) (189, 152.09) (190, 161.70) (191, 178.38) (192, 202.39) (193, 258.51) (194, 294.16) (195, 234.74) (196, 229.14) (197, 227.72) (198, 227.81) (199, 230.78) (200, 228.85) (201, 228.67) (202, 229.57) (203, 230.44) (204, 234.50) (205, 230.09) (206, 236.33) (207, 236.75) (208, 234.53) (209, 235.96) (210, 237.36) (211, 238.81) (212, 240.32) (213, 242.69) (214, 243.54) (215, 245.56) (216, 246.36) (217, 248.46) (218, 352.78) (219, 356.94) (220, 361.74) (221, 361.32) (222, 360.48) (223, 373.38) (224, 371.10) (225, 366.53) (226, 372.82) (227, 362.87) (228, 368.63) (229, 371.56) (230, 370.40) (231, 369.79) (232, 369.23) (233, 368.25) (234, 373.77) (235, 379.25) (236, 374.60) (237, 375.52) (238, 373.60) (239, 376.29) (240, 379.74) (241, 377.83) (242, 379.00) (243, 377.44) (244, 377.35) (245, 381.56) (246, 379.99) (247, 385.52) (248, 426.73) (249, 380.56) (250, 381.95) (251, 380.60) (252, 383.96) (253, 385.20) (254, 386.39) (255, 386.12) (256, 386.43) (257, 394.37) (258, 388.25) (259, 396.43) (260, 391.41) (261, 397.64) (262, 396.29) (263, 395.33) (264, 392.54) (265, 392.30) (266, 398.68) (267, 401.80) (268, 406.82) (269, 405.69) (270, 408.43) (271, 406.97) (272, 404.85) (273, 411.23) (274, 408.37) (275, 417.02) (276, 412.32) (277, 417.00) (278, 415.53) (279, 416.42) (280, 417.93) (281, 418.76) (282, 422.64) (283, 431.16) (284, 424.37) (285, 426.02) (286, 432.69) (287, 432.37) (288, 429.22) (289, 433.18) (290, 438.05) (291, 441.46) (292, 446.58) (293, 452.47) (294, 452.78) (295, 453.73) (296, 458.45) (297, 465.28) (298, 465.74) (299, 466.43) (300, 467.90) (301, 471.38) (302, 472.07) (303, 480.58) (304, 491.63) (305, 489.12) (306, 496.94) (307, 511.02) (308, 514.17) (309, 522.63) (310, 541.68) (311, 561.69) (312, 578.71) (313, 612.21) (314, 655.36) (315, 693.87) (316, 672.18) (317, 680.43) (318, 683.08) (319, 692.33) (320, 684.05) (321, 693.33) (322, 687.25) (323, 701.66) (324, 698.96) (325, 710.20) (326, 705.71) (327, 714.75) (328, 708.78) (329, 731.75) (330, 723.42) (331, 726.65) (332, 725.49) (333, 721.98) (334, 731.07) (335, 729.69) (336, 737.04) (337, 738.16) (338, 726.62) (339, 729.76) (340, 733.27) (341, 741.62) (342, 730.04) (343, 735.40) (344, 745.96) (345, 751.96) (346, 778.82) (347, 747.64) (348, 759.48) (349, 755.16) (350, 790.63) (351, 753.12) (352, 759.48) (353, 757.34) (354, 765.96) (355, 767.32) (356, 774.25) (357, 809.67) (358, 784.38) (359, 771.62) (360, 785.07) (361, 778.72) (362, 786.69) (363, 788.12) (364, 802.40) 		

	    };
	\end{axis}
    \end{tikzpicture}
\end{figure}

\subsection{Analysis}
\paragraph{} As depth increases, floating-point precision must also increase to prevent visual artefacts. In my implementation, precision is determined by a palette; it could be tuned to be finer-grained, and therefore run with less unnecessary precision. I believe that the distinct ``levels'' of difficulty in the graph represent the values in the precision palette.
\paragraph{} There are some unusually high values. It could be the yellow content, but because each frame is similar to the previous and next, there should be a smoother curve.
\paragraph{} Beside the unusually high values, this graph neatly depicts frame difficulty doubling from zoom $2^{-1}$ to $2^{-400}$.
\paragraph{} It is worth noting that this test exceeded walltime (3h30m of walltime for 368 frames); in tracking its progress with \verb=tail=, I observed the work-distribution problem discussed in \ref{possible-mpi-solution}. It limited the number of frames being drawn, so making many of the available threads redundant. With this problem fixed (modifying the distribution algorithm), I think this job would probably have completed, even though each frame was becoming much more expensive.


\newpage
\section{Image resolution}
\paragraph{} When considering the program as a solution to user needs -- that is, as a program to make fractal videos -- it's worth looking at the resolution of the output video. So this test looks at how draw time scales with resolution.

\paragraph{} Because the draw implementation (the serial part) uses a nested loop for the x- and y-coordinates, resolution can be expected to be proportional to the x- and y-size. Because the program only accepts square resolutions, this is simply a square relationship.

\paragraph{} Framerate is also good to know, but can be extrapolated from existing data: it's just a question of frame count.

\paragraph{} Total time for the job is of interest, but average time per frame is also valuable. So I wrote the following one-liner in bash:
\label{bash-avg-func}\begin{figure}
    \caption{Bash function to determine average CPU time per frame}
    \begin{verbatim}
    $ avg() { grep "completed" $1 | awk '{sum=sum+$10;}END{print sum/NR;}'; }
    $ avg resolution-1440.o26938
    34.5186
    \end{verbatim}
\end{figure}
\paragraph{} It takes a log file and prints a single decimal value, which is included in the plot.

\paragraph{} So there are two time plots, for total wall time and average CPU time:\\

\begin{figure}
    \caption{Testing parameters for image resolution}
    \begin{verbatim}
    export RES=640 # our testing variable
    export ITERS=2000
    export STARTZOOM=50
    export ENDZOOM=100
    export FRAMES=50
    export OUTPUT_DIRECTORY=img
    \end{verbatim}
\end{figure}
\begin{figure}
    \begin{tikzpicture}
	\begin{axis}[
		xtick=data,
		xlabel=Resolution (pixels$^2$), ylabel=Time (s),
		ymin=0,
		width=300,
		xmin=320,
		xmax=2880,
		title=Wall time with variable resolution
	    ]
	    \addplot coordinates {
		(320,164)
		(640,167)
		(1024,421)
		(1440,823)
		(1920,1450)
		(2880,3295)
	    };
	\end{axis}
    \end{tikzpicture}\\
\end{figure}
\begin{figure}
    \begin{tikzpicture}
	\begin{axis}[
		xtick=data,
		xlabel=Resolution (pixels$^2$), ylabel=Time (s),
		ymin=0,
		width=300,
		xmin=320,
		xmax=2880,
		title=Average CPU time per frame with variable resolution
	    ]
	    \addplot[color=red, mark=*] coordinates {
		(320,6.7664)
		(640,6.8702)
		(1024, 17.625)
		(1440,34.5186)
		(1920,61.14)
		(2880,139.002)
	    };
	\end{axis}
    \end{tikzpicture}
\end{figure}

\subsection{Analysis}
Note that 2880 is double 1440, and takes quadruple its time: draw time scales with the square of resolution, which is what I expected.

The lower end highlights a quirk of my implementation:
I implemented the program expecting that individual frames would take significant time, and so there is some overhead which will appear at lower resolutions. Reallocating each variable per frame is tiny compared to a 500-second frame, but for a 2-second frame it's significant.
The program doesn't need this overhead, it's just that I was only optimising expected hotspots.

\newpage
\section{Memory}
I tracked memory usage across all tests, and it seems to hit a cap at around 200MB per slave. I speculate that this is a combination of using a limited number of floating-point variables, which are relatively small (512 bits is a lot of precision but a tiny amount of memory), and having a limited number of threads running (8 threads on Savanna).
\footnote{Data here would literally be ``here are twenty tests each with 200MB usage per slave'' -- a flat graph}

With deeper zooms using higher precision, it is likely that this would increase, but not greatly, especially within the context of Savanna and Barrine's RAM sizes.


\newpage
\section{Scientific experiment}
\label{sec:scientific-experiment}
The task as specified was to write a program to create a fractal zoom movie.

This project implements the Mandelbrot fractal with fixed co-ordinates and a custom colourscheme.

Because of the deterministic nature of a fractal, there is very little input required. This is reflected in the MPI communication needed by the parallel component: there is only a little of it, and it is very small bytewise. Drawing a frame is based on the algorithm implemented within \verb=get_mandelbrot=, and the rest of the project falls into the following tasks:
\begin{itemize}
    \item{} Create and draw frames in order to create a video (interpolation of zoom values)
    \item{} Distribute work between workers to share the drawing load (scatter)
    \item{} Gather resulting frames together (restore order)
    \item{} Stitch into video (FFmpeg)
\end{itemize}

\subsection{Fractal zoom movie}
\subsubsection{Fractal}
A fractal is a mathematical pattern with self-similarity and, often, surprising detail at high magnification/zoom.
The Mandelbrot fractal, implemented by this project, uses two dimensions ($x$ and $y$) to represent complex numbers relating to the Mandelbrot sequence.
Each pixel in the fractal can be coloured on a variety of properties -- this implementation colours pixels based on the number of iterations required to escape the Mandelbrot sequence.
\subsubsection{Zoom}
At the start of a video, the frame's boundaries might go from $(-1,-1)$ to $(1,1)$, with width $2$, or $2^{1}$.\footnote{The exact values depend on the co-ordinates at the centre of the frame, and do not represent the co-ordinates used in this project for simplicity's sake.} As the camera zooms in, the frame will go from $(-0.5,-0.5)$ to $(0.5,0.5)$, with width $1$ or $2^{0}$. Zoom further, and the boundaries are at $0.125$, width $0.25=2^{-2}$. This power of negative two is the ``zoom factor''.
Eventually, standard floating-point stops being able to tell the difference between numbers at some scale (zoom): it needs more bits for precision. So we use an arbitrary-precision library. \cite{MPFR}
\subsubsection{Movie}
The free utility FFmpeg \cite{ffmpeg} can make a video from PNG files with ordered names: fractal001.png, fractal002.png, and so on. The program outputs frames with this naming scheme.
FFmpeg has excellent multithreading support and will stitch together 2,000 frames on my laptop in under 5 minutes -- 2,000 frames generally take upwards of 15 CPU hours to produce, so it's a small component of the overall time. Moss can also run FFmpeg.

\subsection{The drawing component}
The co-ordinates used for the final program are very precise (300 decimal digits).
The drawing component is the only expensive part of the whole program: everything else is very lightweight.
It is a simple nested loop: each pixel is evaluated independently, although there are column and row variables in common.
Each pixel has a value determined for it: the number of iterations required to escape the Mandelbrot set.
My implementation compartmentalises the drawing into a single function: it takes parameters (zoom etc) and returns a 2D array of iterations.\footnote{Technically a 1D array with j*y+x accessing, as is good for performance in C}
The array is then put through a colouring algorithm (which is modulus-based) and written as a PNG.

\subsection{OpenMP}
OpenMP is implemented by allowing processes to spawn a thread for each frame. It works very well, with very little interference.

\subsection{OpenMPI}
OpenMPI is used to distribute work between processes. The scientific problem is such that there is very little information required to wholly describe a chunk of work, and so my program doesn't experience any MPI crowding at current scales. I don't know whether it would apply if every node on Barrine were used at once, or if it were run on a huge cluster, but I suspect it would scale better than many scientific problems.

The master has very little work to do: it parses startup data before the job gets underway, breaks off chunks of work (very cheap), and handles requests for more work. It busy-waits for a request most of the time, with a few kilobytes of variables in memory. So it only uses one core on one node.

Towards the end of the job, as little work remains to be done, my program deliberately increases the frequency of MPI communications to ensure that every rank remains working until the end.

The lack of need for any inter-slave communication makes the model straightforward -- conceptually, the job will ``scatter' instructions and ``gather'' frames, but PBS and the network filesystem take care of the gathering. It would be worth implementing that within the program: save to \verb=scratch= and dump tarballed images sporadically, or at the end. It is useful to inspect images as they are finished, though.

\newpage
\section{Conclusions}
It's a mixed bag of success and failure. OpenMP and resolution scale exactly in line with the theory, but strong and weak scaling have unexpected behaviour and I've had only partial success identifying why. Further investigation (by altering the algorithm) may reveal much better (and more expected) scaling.\\
However, in investigating these issues, I've revised the theory, thought about the workings of the programs and machines, and tried a lot of techniques.\\
The project has also been good practice in general tool use: dealing with module problems, failed transmissions and compilation issues isn't formally part of the subject material but is essential for operating in the subject.


\clearpage
\newpage
\begin{thebibliography}{99}
\bibitem{ultrazoom}
"Mandelbrot Ultra Zoom \#5: 2.1E275": Orson Wang, 2010
http://fractaljourney.blogspot.com.au/2010/01/mandelbrot-ultra-zoom-5-21e275.html
\bibitem{wiki-mandelbrot-set}
Mandelbrot set - Wikipedia
http://en.wikipedia.org/wiki/Mandelbrot\_set\#Escape\_time\_algorithm
\bibitem{MPFR}
The GNU MPFR Library
http://www.mpfr.org
\bibitem{ffmpeg}
FFmpeg
https://www.ffmpeg.org
\end{thebibliography}

\end{document}
