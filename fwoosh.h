#ifndef MAIN_H__
#define MAIN_H__
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <limits.h>
#include <time.h>
#include <stdio.h> // must be before mpfr
#include <getopt.h>
#include <gmp.h>
#include <mpfr.h>
#include <mpi.h>

#define YRES 1024
#define XRES 1024

// MPI tags and signal codes
#define TAG_INITIAL 50		// can be OK or KILL - may actually be deprecated by MPI_Bcast
#define SIGNAL_OK 0		// value for TAG_INITIAL
#define SIGNAL_KILL 1		// value for TAG_INITIAL
#define TAG_CHUNK 99		// master - tag meaning "here is a chunk"
#define TAG_DIRECTORY_PATH 86   // master - tag meaning "move to this directory"
#define TAG_WORK_COMPLETE 26	// slave - tells master that it's completed a chunk
#define TAG_DISTRIBUTE 13	// master - can be "sending a chunk" or "finalize"
#define SIGNAL_MORE_WORK 0	// value for TAG_DISTRIBUTE
#define SIGNAL_FINALIZE 1	// value for TAG_DISTRIBUTE

#define BASESIZE 16.0	// when zoom is 0

#define MAX_PATH_LENGTH 100 // file path to output images to

typedef struct pixel {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} pixel;

typedef struct coords_t {
    // coordinates are hardcoded and therefore super precise
    mpfr_t r; // real aka x
    mpfr_t i; // imaginary aka y
    // start and dest are transmitted as doubles
    mpfr_t start;   // two magnification/zoom values
    mpfr_t dest;    // two magnification/zoom values
    mpfr_prec_t bits; // current precision of all mpfr_t vars
} coords_t;

/*
 * A chunk of frames. Precision will be determined by palette.
 * An image requires the following parameters: coordinates, zoom factor, resolution and maximum iterations.
 * A requirement implied by zoom factor is the floating-point precision to be used.
 * Resolution is common across all images in a video -
 *  - it could be varied, but would make little sense in the final product.
 * Coordinates are constant across the video; in fact, they are hardcoded for simplicity's sake.
 * So, then, we can fully describe the work in a chunk using the six variables below.
 */
typedef struct chunk_t {
    int resolution;	// square resolution
    int maxIters;	// mandelbrot iterations
    double zoomStart;	// magnification
    double zoomEnd;	// magnification
    int frames;		// how many frames to spread this across
    int startFrame;	// frame number to start on (for img output)
} chunk_t;

/*
 * Mallocs the chunk datatype for MPI's use.
 */
MPI_Datatype* create_chunk_datatype(void);

/*
 * Startup function for master before the main program loop.
 * Calls argument parsing and change directory. If either fails,
 * will broadcast a kill message to the slaves.
 * If this function returns, our input validation is successful.
 */
void master_init(int mpiSize, int argc, char **argv, chunk_t *allChunk);

/*
 * Just prints a chunk.
 */
void print_chunk(chunk_t c, int rank);

/*
 * Divides up the allChunk into work for each slave.
 * Exactly *how* it does this is a question of algorithms.
 * Should leave something in allChunk too.
 * Modifies allChunk to contain the remainder of the work.
 * Returns an array of chunks of size mpiSize-1.
 * Complement of send_out_initial_work.
 */
chunk_t* divide_initial_work(int mpiSize, chunk_t *allChunk);

/*
 * Takes an array of chunks, of size mpiSize-1, and sends them to the slave ranks.
 * If a send fails, it does nothing, but could usefully kill the rest and exit.
 * Frees the chunk array at the end.
 * Complement of divide_initial_work.
 */
int send_out_initial_work(int mpiSize, chunk_t *chunks, MPI_Datatype *mpi_chunk_t);

/*
 * Distributes the same directory path to every rank.
 * Master has populated *directory, others have not.
 * Directory path is for image output.
 */
int distribute_directory(char *directory);

/*
 * Handles distribution of the work after initial distribution.
 */
void redist_loop(int mpiSize, chunk_t *allChunk, MPI_Datatype *mpi_chunk_t);

/*
 * Algorithm!
 * Removes a section of work from allChunk and returns it as a chunk.
 * May empty out allChunk.
 * Breaks off chunks in shallow->deep zoom order.
 */
chunk_t break_off_chunk(chunk_t *allChunk);

/*
 * Prints the contents of a coord struct.
 */
void print_coord(char* name, coords_t c);

/*
 * Initialises a coords struct.
 * Don't forget to free it with free_coords().
 * Includes the hard-coded fractal coordinates.
 */
coords_t* set_coords(chunk_t myChunk);

/*
 * Clears out a coords struct and frees it.
 */
void free_coords(coords_t *coords);

/*
 * Parses arguments and returns SIGNAL_OK (if good) or SIGNAL_KILL (if bad).
 * Prints usage instructions if bad.
 */
int parse_args(int argc, char** argv, chunk_t *allChunk, char **directory);

/*
 * We output our files into a directory specified in the parameters.
 * This function sets the program's working directory to be there.
 * If the directory does not exist, function will fail and return 1.
 */
int change_directory(char* dest);

#include "png.h"
#include "fractal.h"
#include "slave.h"

#endif
