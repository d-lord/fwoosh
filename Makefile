# don't touch
CFLAGS=-std=gnu99 -Wall -pedantic -g -O3
DISABLED_CFLAGS=-pg
CODE_FILES=Makefile fwoosh.c fwoosh.h png.c png.h fractal.c fractal.h slave.c slave.h
LATEX_FILES= report.tex latex/*
INCLUDE= -I thirdparty/include/
LIB=-lpng -Lthirdparty/lib -lmpfr -lgmp -fopenmp -lrt

# config:

# cluster to transmit files to (including the right .sh script)
# cluster can be moss - bit of a misnomer in that case
CLUSTER=savanna
# leave blank if not using barrine - otherwise {1,2,3} for login node
NODE=
# the folder's location on the cluster
DIR=~/fwoosh
# the subfolder to save output files (images) into
OUTPUT_DIR=img
VIDNAME = out.mp4
VIDFPS=30

default: 	main

fwoosh.o:	fwoosh.c fwoosh.h png.h slave.h
		mpiicc -c fwoosh.c -o fwoosh.o $(CFLAGS) $(INCLUDE) $(LIB)

png.o:		png.c png.h
		mpiicc -c png.c -o png.o $(CFLAGS) $(INCLUDE) $(LIB)

fractal.o:	fractal.c fractal.h
		mpiicc -c fractal.c -o fractal.o $(CFLAGS) $(INCLUDE)

slave.o:	slave.c slave.h fractal.h
		mpiicc -c slave.c -o slave.o $(CFLAGS) $(INCLUDE) $(LIB)

main:		fwoosh.o png.o fractal.o slave.o
		mpiicc fwoosh.o png.o fractal.o slave.o $(CFLAGS) $(INCLUDE) $(LIB) -o fwoosh

clean:		clean-compiled

clean-compiled:
		rm -f fwoosh.o png.o fractal.o slave.o fwoosh fwoosh.tar.bz2

clean-all:	clean clean-logs clean-output

# shell script logs from the clusters
clean-logs:
		rm -f fwoosh.{e,o}* run-$(CLUSTER).sh.{e,o}*

# images/videos in main dir
clean-output:
		rm -f $(OUTPUT_DIR)/* $(VIDNAME)

# potential optimisation: can we run ffmpeg concurrently, in a way
# which uses resources not otherwise fractaling?
# Moss doesn't have ffmpeg. I don't remember which cluster offers a module.
animation:
		rm -f $(VIDNAME)
		ffmpeg -r $(VIDFPS) -i $(OUTPUT_DIR)/img%06d.png -c:v libx264 -pix_fmt yuv420p $(OUTPUT_DIR)/$(VIDNAME)

# all files required to compile
transmit:       $(CODE_FILES)
		tar -jcf fwoosh.tar.bz2 $(CODE_FILES) tests
		rsync -avz fwoosh.tar.bz2 $(CLUSTER)$(NODE):$(DIR)/
		sleep 1s # moss doesn't seem to like two so close together
		ssh $(CLUSTER)$(NODE) 'cd $(DIR); tar -jxvf fwoosh.tar.bz2'

# latex files (probably to go into subversion)
transmit-latex:	$(LATEX_FILES)
		tar -jcf fwoosh-tex.tar.bz2 $(LATEX_FILES)
		rsync -avz fwoosh-tex.tar.bz2 $(CLUSTER)$(NODE):$(DIR)/
		#sleep 1s # moss doesn't seem to like two so close together
		ssh $(CLUSTER)$(NODE) 'cd $(DIR); tar -jxvf fwoosh-tex.tar.bz2'

fetch:
		ssh $(CLUSTER)$(NODE) 'cd $(DIR)/$(OUTPUT_DIR); tar -jcf images.tar.bz2 img*.png'
		rsync -e ssh $(CLUSTER)$(NODE):$(DIR)/$(OUTPUT_DIR)/images.tar.bz2 fetched
		tar -jvxf fetched/images.tar.bz2 -C $(OUTPUT_DIR)
		ssh $(CLUSTER)$(NODE) 'cd $(DIR)/$(OUTPUT_DIR); rm images.tar.bz2'

report:
		pdflatex report.tex && pdflatex report.tex
		rm report.aux report.log

# change -C . to -C img once I've made `make animation` go in there
