#include "slave.h"

/*
 * The main drawing loop for a slave process.
 * Returns once the slave has completed its chunk.
 * Uses OpenMP to thread the task.
 */
int slave_draw_loop(chunk_t myChunk, coords_t *coords, int mpiRank, int mpiSize) {
    int bmpX = myChunk.resolution; // shared across all loops
    int bmpY = myChunk.resolution; // shared across all loops
    int frameNo;
    pixel* bmp; // per-thread: contains one image
    struct timespec start, end;
    clock_getres(CLOCK_THREAD_CPUTIME_ID, &start);
    clock_getres(CLOCK_THREAD_CPUTIME_ID, &end);

#pragma omp parallel for private(bmp, start, end) shared(myChunk) schedule(dynamic)
    for (frameNo = 0; frameNo < myChunk.frames; frameNo++)
    {
	bmp = malloc(sizeof(pixel*)*bmpY*bmpX);

	if (frameNo%PROGRESS==0)  {
	    mpfr_printf("Rank %d drawing frameNo %d, overall frame %d\n", mpiRank, frameNo, frameNo+myChunk.startFrame);
	}
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
	make_mandelbrot_on_frame(bmp, bmpX, bmpY, coords, frameNo,
		myChunk.frames, myChunk.maxIters);
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
	double elapsed = (end.tv_sec - start.tv_sec) + (((double)end.tv_nsec - start.tv_nsec)/1000000000);
	mpfr_printf("Rank %d completed frameNo %d, overall frame %d, "
		"in %0.2f CPU seconds\n", mpiRank, frameNo, frameNo+myChunk.startFrame,
		elapsed);

#pragma omp critical
	{
	    // TODO: critical?
	    write_png(bmp, bmpX, bmpY, myChunk.startFrame+frameNo);
	}

	free(bmp);
    }

    // We free coords because if we receive another chunk, it's likely to be at
    // another precision anyway
    free_coords(coords);

    return 0;
}

/*
 * Gets startup data.
 * Specifically, a signal for whether the job will run or not,
 * and a directory to put images in.
 * Job may not run if the argument parsing fails.
 * Returns either SIGNAL_OK or SIGNAL_KILL, and assigns into directory.
 */
int get_startup_data(char** directory) {
    int signal; // SIGNAL_OK or SIGNAL_KILL
    MPI_Status status;
    int mpiRank;
    MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
    MPI_Bcast(&signal, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (signal == SIGNAL_KILL) {
	return signal;
    }
    int chars; // number of characters received
    MPI_Probe(0, TAG_DIRECTORY_PATH, MPI_COMM_WORLD, &status);
    MPI_Get_count(&status, MPI_CHAR, &chars);
    char *directory_recv = (char*) malloc((chars+1)*(sizeof(char)));
    if (MPI_Recv(directory_recv, chars, MPI_CHAR, 0, TAG_DIRECTORY_PATH,
		MPI_COMM_WORLD, &status) != MPI_SUCCESS) {
	fprintf(stderr, "Non-master process %d failed to receive chunk\n", mpiRank);
	return 1;
    }
    directory_recv[chars] = '\0';
    *directory = directory_recv;
    return SIGNAL_OK;
}


/*
 * Called when the current chunk has been fully drawn.
 * Notifies the master rank that this rank has completed its work.
 * Master will send a signal in response, but that's not part of this function:
 * see get_chunk().
 */
void notify_master(void) {
    MPI_Send(NULL, 0, MPI_CHAR, 0, TAG_WORK_COMPLETE, MPI_COMM_WORLD);
}


/*
 * Called when this rank needs a new chunk of work.
 * This is true both for initial and redistributed work.
 * Master will send a signal, then any work.
 * Either master will send new work, or master will send a "game over" signal.
 * This function receives the signal and receives&assigns the chunk.
 * Returns either SIGNAL_MORE_WORK or SIGNAL_FINALIZE.
 */
int get_chunk(chunk_t *myChunk, MPI_Datatype mpi_chunk_t) {
    MPI_Status status;
    int signal; // first sent, then received
    signal = 0;
    MPI_Send(&signal, 1, MPI_INT, 0, TAG_WORK_COMPLETE,
	    MPI_COMM_WORLD);
    MPI_Recv(&signal, 1, MPI_INT, 0, TAG_DISTRIBUTE,
	    MPI_COMM_WORLD, &status);
    if (signal == SIGNAL_FINALIZE) {
	// no work for this process
	return SIGNAL_FINALIZE;
    } else {
	// receive and update the chunk
	signal = MPI_Recv(myChunk, 1, mpi_chunk_t, 0,
		TAG_CHUNK, MPI_COMM_WORLD, &status);
	if (signal != MPI_SUCCESS) {
	    fprintf(stderr, "Slave process failed to receive chunk\n");
	    return SIGNAL_FINALIZE;
	}
	return SIGNAL_MORE_WORK;
    }
    return 0;
}
