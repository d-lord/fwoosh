#include "fwoosh.h"

const struct option long_options[] = {
    {"resolution", required_argument, NULL, 'r'},
    {"max-iters", required_argument, NULL, 'i'},
    {"start-zoom", required_argument, NULL, 's'},
    {"end-zoom", required_argument, NULL, 'e'},
    {"frames", required_argument, NULL, 'f'},
    {"path", required_argument, NULL, 'p'},
    {0,  0, 0, 0} // required by getopt_long
};

/*
 * Usage: see the string in parse_args or run program without parameters
 * Suggested parameters:
 * fwoosh -r 1024 -i 1000 -s 1 -e 50 -f 20 -d img
 *
 * Zoom point is configurable by editing set_coords. Not accessible via command-line.
 */
int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);
    int mpiRank, mpiSize;
    MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
    MPI_Datatype mpi_chunk_t = *create_chunk_datatype();
    MPI_Type_commit(&mpi_chunk_t);
    printf("Hello world from rank %d of %d\n", mpiRank, mpiSize);

    if (mpiRank == 0) { // master
	chunk_t allChunk; // contains all the work to be done
	master_init(mpiSize, argc, argv, &allChunk);
	chunk_t *initialChunks; // of size mpiSize-1
	initialChunks = divide_initial_work(mpiSize, &allChunk);
	// allChunk is now the remaining work - think of it as remainingWorkChunk
	if (send_out_initial_work(mpiSize, initialChunks, &mpi_chunk_t)) {
	    fprintf(stderr, "Master left send_out_initial_work() unhappy");
	    MPI_Finalize();
	    return 1;
	}
	print_chunk(allChunk, 0);
	redist_loop(mpiSize, &allChunk, &mpi_chunk_t);
	// redist_loop returns when there are no ranks working and it's just finalised the last one
	printf("Master rank 0 has finished!\n");
	MPI_Finalize();
	printf("---------------\n");
	printf("Job finalized\n");
	printf("---------------\n");
	return 0;
    }

    else { // slave
	// init stuff
	chunk_t myChunk;
	char *directory;
	if (get_startup_data(&directory) == SIGNAL_KILL) {
	    fprintf(stderr, "Non-master process %d aborting from bad master init\n", mpiRank);
	    MPI_Finalize();
	    return 1;
	}
	printf("Non-master rank %d received startup data OK\n", mpiRank);
	change_directory(directory);
	printf("Non-master rank %d changed to directory %s\n", mpiRank, directory);

	coords_t *coords;
	// The most important part of the slave: the draw loop
	while (get_chunk(&myChunk, mpi_chunk_t) != SIGNAL_FINALIZE) {
	    print_chunk(myChunk, mpiRank);
	    coords = set_coords(myChunk); // precision, mostly
	    slave_draw_loop(myChunk, coords, mpiRank, mpiSize);
	}

	printf("Slave rank %d has finished!\n", mpiRank);
	free(directory);
	MPI_Finalize();
	return 0;
    }
}

/*
 * Mallocs the chunk datatype for MPI's use.
 */
MPI_Datatype* create_chunk_datatype(void) {
    // it's essentially the option struct
    // int resolution, int maxIters
    // double zoomStart, zoomEnd
    // int frames
    // int startFrame
    const int count = 6; // res, iters, two zooms, frames, startFrame
    int blockLengths[6] = {1,1,1,1,1,1};
    MPI_Datatype types[6] = {MPI_INT, MPI_INT, MPI_DOUBLE, MPI_DOUBLE, 
	MPI_INT, MPI_INT};
    MPI_Datatype *mpi_chunk_t = malloc(sizeof(MPI_Datatype));
    MPI_Aint offsets[7];
    offsets[0] = offsetof(chunk_t, resolution);
    offsets[1] = offsetof(chunk_t, maxIters);
    offsets[2] = offsetof(chunk_t, zoomStart);
    offsets[3] = offsetof(chunk_t, zoomEnd);
    offsets[4] = offsetof(chunk_t, frames);
    offsets[5] = offsetof(chunk_t, startFrame);
    MPI_Type_create_struct(count, blockLengths, offsets, types, mpi_chunk_t);
    return mpi_chunk_t;
}

/*
 * Startup function for master before the main program loop.
 * Calls argument parsing and change directory. If either fails,
 * will broadcast a kill message to the slaves.
 * If this function returns, our input validation is successful.
 */
void master_init(int mpiSize, int argc, char **argv, chunk_t *allChunk) {
    int initialStatus; // for MPI comms
    char* outputPath; // filesystem path to output images
    initialStatus = parse_args(argc, argv, allChunk, &outputPath);
    // if parse succeeded, try to change to that directory
    initialStatus = (((initialStatus==SIGNAL_KILL) || change_directory(outputPath)) 
	    ? SIGNAL_KILL : SIGNAL_OK);
    // if change_directory failed, we get SIGNAL_KILL
    // send whichever signal it is to everyone
    MPI_Bcast(&initialStatus, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (initialStatus == SIGNAL_KILL) {
	fprintf(stderr, "Master process aborting from bad init\n");
	MPI_Finalize();
	exit(1);
    }
    // input validation completed
    distribute_directory(outputPath);
}

/*
 * Divides up the allChunk into work for each slave.
 * Exactly *how* it does this is a question of algorithms.
 * Should leave something in allChunk too.
 * Modifies allChunk to contain the remainder of the work.
 * Returns an array of chunks of size mpiSize-1.
 */
chunk_t* divide_initial_work(int mpiSize, chunk_t *allChunk) {
    /* naming:
     * allocate - determine a share of the work
     * distribute - assign that allocation */
    int i, j;
    int allFrames = allChunk->frames; // a copy which is not modified
    int framesToDistribute = allFrames/4;
    allChunk->frames -= framesToDistribute;
    chunk_t *chunks = (chunk_t*) malloc((mpiSize-1)*sizeof(chunk_t));

    // round-robin allocate frames
    for (i = 0; i < mpiSize-1; i++) {
	chunks[i].frames = 0;
    }
    j = 0;
    // distribute frames from the pool
    for (i = 0; i < framesToDistribute; i++) {
	chunks[j].frames++;
	j = (j+1) % (mpiSize-1);
    }

    // chunks now look something like {16, 16, 15, 15}
    // could also be {1, 1, 0, 0}
    // but not {0, ...} because option parsing requires >0 frames
    // define the startFrame for each, based on the previous

    double magnification_per_frame = (allChunk->zoomEnd - allChunk->zoomStart)/allFrames;
    double prevEnd = allChunk->zoomStart;
    // allocate and distribute zoom coordinates
    for (i = 0; i < mpiSize-1; i++) {
	chunks[i].zoomStart = prevEnd;
	chunks[i].zoomEnd = prevEnd +
	    magnification_per_frame*chunks[i].frames;
	prevEnd = chunks[i].zoomEnd;
    }
    allChunk->zoomStart = prevEnd;

    // define the startFrame for each, based on the previous
    int sum = 0;
    for (i = 0; i < mpiSize-1; i++) {
        chunks[i].startFrame = sum;
        sum += chunks[i].frames;
    }
    allChunk->startFrame = sum;

    // distribute shared values to the chunks
    for (i = 0; i < mpiSize-1; i++) {
	// these could be done as memcpy, but this is easier
	chunks[i].resolution = allChunk->resolution;
	chunks[i].maxIters = allChunk->maxIters;
    }
    return chunks;
}

/*
 * Takes an array of chunks, of size mpiSize-1, and sends them to the slave ranks.
 * If a send fails, it does nothing, but could usefully kill the rest and exit.
 * Frees the chunk array at the end.
 * Returns 1 on failure, 0 on success.
 */
int send_out_initial_work(int mpiSize, chunk_t *chunks, MPI_Datatype *mpi_chunk_t) {
    // send out chunks asynchronously!
    int i;
    int recipients = mpiSize-1;
    MPI_Request *sends = malloc(sizeof(MPI_Request)*recipients);
    MPI_Status *statuses = malloc(sizeof(MPI_Status)*recipients);
    int status = SIGNAL_MORE_WORK;
    fflush(stdout);
    for (i = 0; i < recipients; i++) {
	MPI_Isend(&status, 1, MPI_INT, i+1, TAG_DISTRIBUTE,
		MPI_COMM_WORLD, &sends[i]);
    }
    if (MPI_Waitall(recipients, sends, statuses) == MPI_ERR_IN_STATUS) {
	fprintf(stderr, "Master: One or more TAG_DISTRIBUTE sends failed,"
		"but taking no action\n"); // this could be improved, really
	// http://www.mpich.org/static/docs/v3.1/www3/MPI_Waitall.html
	return 1;
    }
    for (i = 0; i < recipients; i++) {
	MPI_Isend(&chunks[i], 1, *mpi_chunk_t, i+1, TAG_CHUNK,
		MPI_COMM_WORLD, &sends[i]);
    }
    if (MPI_Waitall(recipients, sends, statuses) == MPI_ERR_IN_STATUS) {
	fprintf(stderr, "Master: One or more chunk sends failed,"
		"but taking no action\n"); // this could be improved, really
	// http://www.mpich.org/static/docs/v3.1/www3/MPI_Waitall.html
	return 1;
    }
    // receive and discard the first TAG_WORK_COMPLETE from slaves' get_chunk()
    int garbage;
    MPI_Status no;
    for (i = 0; i < recipients; i++) {
	MPI_Recv(&garbage, 1, MPI_INT, i+1, TAG_WORK_COMPLETE, MPI_COMM_WORLD, &no);
    }
    free(chunks);
    free(sends);
    free(statuses);
    return 0;
}

/*
 * Parses arguments and returns SIGNAL_OK (if good) or SIGNAL_KILL (if bad).
 * Prints usage instructions if bad.
 */
int parse_args(int argc, char** argv, chunk_t *allChunk, char **directory) {
    opterr = 0;
    int option_index = 0;
    int c; // getopt's returned argument
    long int tempInt;
    double tempDouble;
    *directory = NULL;

    allChunk->resolution = allChunk->maxIters = allChunk->frames = 0; // sentinel
    allChunk->zoomStart = allChunk->zoomEnd = 0.0; // sentinel

    while ((c = getopt_long(argc, argv, 
		    "r:i:s:e:f:d:",
		    long_options, &option_index)) != -1) {
	switch(c) {
	    case 'r':
		tempInt = strtol(optarg, NULL, 0xA);
		if (tempInt > INT_MAX || tempInt < 1) {
		    fprintf(stderr, "Bad resolution\n");
		    goto bad;
		}
		allChunk->resolution = tempInt;
		// note that the resolution is stored in both bmp and allChunk
		break;
	    case 'i':
		tempInt = strtol(optarg, NULL, 0xA);
		if (tempInt > INT_MAX || tempInt < 1) {
		    fprintf(stderr, "Bad max-iters\n");
		    goto bad;
		}
		allChunk->maxIters = tempInt;
		break;
	    case 's':
		tempDouble = strtod(optarg, NULL);
		allChunk->zoomStart = tempDouble; 
		// if no conversion is performed by strtod, it returns 0
		// which is checked before return
		break;
	    case 'e':
		tempDouble = strtod(optarg, NULL);
		allChunk->zoomEnd = tempDouble;
		break;
	    case 'f':
		tempInt = strtol(optarg, NULL, 0xA);
		if (tempInt > INT_MAX || tempInt < 1) {
		    fprintf(stderr, "Bad frames\n");
		    goto bad;
		}
		allChunk->frames = tempInt;
		break;
	    case 'd':
		*directory = malloc(sizeof(char)*(strlen(optarg)+1));
		strcpy(*directory, optarg);
		(*directory)[strlen(optarg)] = '\0';
		if (strlen(*directory) > MAX_PATH_LENGTH) {
		    fprintf(stderr, "--directory exceeds %d chars\n", MAX_PATH_LENGTH);
		    free(directory);
		    goto bad;
		}
		if (strlen(*directory) == 0) { // required_argument may make this redundant
		    fprintf(stderr, "No --directory\n");
		    free(directory);
		    goto bad;
		}
		break;
	    case '?':
		fprintf(stderr, "Unknown argument: %c\n", optopt);
		goto bad;
	    default:
		fprintf(stderr, "Hit default getopt case somehow\n");
		goto bad;
	}
    }
    // closing-shop checks
    if (allChunk->resolution && allChunk->maxIters && allChunk->zoomStart && 
	    allChunk->zoomEnd && allChunk->frames && *directory != NULL) {
#ifdef DEBUG
	printf("Exiting happily with the following arguments:\n");
	printf("r = %d\n", bmp->x);
	printf("i = %d\n", allChunk->maxIters);
	printf("s = %f\n", allChunk->zoomStart);
	printf("e = %f\n", allChunk->zoomEnd);
	printf("f = %d\n", allChunk->totalFrames);
	printf("d = %s\n", *directory);
#endif
	return SIGNAL_OK;
    } else {
	fprintf(stderr, "One or more arguments missing or zero.\n");
	goto bad;
    }
bad:
    fprintf(stderr, "Fwoosh requires all of the following options:\n"
	    "--resolution or -r\n"
	    "--max-iters or -i\n"
	    "--start-zoom or -s\n"
	    "--end-zoom or -e\n"
	    "--frames or -f\n"
	    "--directory or -d\n");
    fprintf(stderr, "Coordinates are hardcoded. Sorry. Change the strings.\n");
    return SIGNAL_KILL;
}

/*
 * Initialises a coords struct.
 * Don't forget to free it with free_coords().
 * Includes the hard-coded fractal coordinates.
 * Precision is determined by the deepest frame in this chunk.
 */
coords_t* set_coords(chunk_t myChunk) {
    coords_t *coords = malloc(sizeof(coords_t));
    coords->bits = get_precision_at_zoom(myChunk.zoomEnd);
    mpfr_inits2(coords->bits, coords->r, coords->i, coords->start,
	    coords->dest, (mpfr_ptr) NULL);
    mpfr_set_d(coords->start, myChunk.zoomStart, MPFR_RNDD);
    mpfr_set_d(coords->dest, myChunk.zoomEnd, MPFR_RNDD);
    /*
     * these coords are all right, but not especially precise
    mpfr_set_str(coords->r,
	    "0.2636878909832002",
	    0xA,
	    MPFR_RNDD);
    mpfr_set_str(coords->i,
	    "0.002364174739363993",
	    0xA,
	    MPFR_RNDD);
    */
    /*
     * the coordinates below are from the following deep zoom:
     * http://fractaljourney.blogspot.com.au/2010/01/mandelbrot-ultra-zoom-5-21e275.html 
     * I've had difficulty finding sufficiently precise coordinates,
     * and searching for them within the program is impractical - so
     * this coordinate set is the only deep-zoom test I have
     */
    mpfr_set_str(coords->r,
	"-1.740062382579339905"
	"220844167065825638296"
	"641720436171866879862"
	"418461182919644153056"
	"054840718339483225743"
	"450008259172138785492"
	"983677893366503417299"
	"549623738838303346465"
	"461290768441055486136"
	"870719850559269507357"
	"211790243666940134793"
	"753068611574745943820"
	"712885258222629105433"
	"648695946003865"
	    , 0xA // base ten
	    , MPFR_RNDD);
    mpfr_set_str(coords->i,
	"0.0281753397792110489"
	"924115211443195096875"
	"390767429906085704013"
	"095958801743240920186"
	"385400814658560553615"
	"695084486774077000669"
	"037710191665338060418"
	"999324320867147028768"
	"983704831316527873719"
	"459264592084600433150"
	"333362859318102017032"
	"958074799966721030307"
	"082150171994798478089"
	"798638258639934"
	, 0xA // base ten
	, MPFR_RNDD);
    return coords;
}

/*
 * Clears out a coords struct and frees it.
 */
void free_coords(coords_t *coords) {
    if (!coords) {
	return;
    }
    mpfr_clears(coords->r, coords->i, coords->start, coords->dest,
	    (mpfr_ptr) NULL);
    ;
}

/*
 * Prints the contents of a coord struct.
 */
void print_coord(char* name, coords_t c) {
    printf("Contents of %s:\n", name);
    printf("%s.bits: %lu\n", name, (unsigned long) c.bits);
    printf("%s.r: ", name);
    mpfr_out_str(NULL, 0xA, c.bits, c.r, MPFR_RNDD);
    putchar('\n');
    printf("%s.i: ", name);
    mpfr_out_str(NULL, 0xA, c.bits, c.i, MPFR_RNDD);
    putchar('\n');
    printf("%s.start: ", name);
    mpfr_out_str(NULL, 0xA, c.bits, c.start, MPFR_RNDD);
    printf("%s.dest: ", name);
    mpfr_out_str(NULL, 0xA, c.bits, c.dest, MPFR_RNDD);
    putchar('\n');
}

/*
 * Just prints a chunk.
 */
void print_chunk(chunk_t c, int rank) {
    printf("Rank %d chunk: resolution: %d\n", rank, c.resolution);
    printf("Rank %d chunk: maxIters: %d\n", rank, c.maxIters);
    printf("Rank %d chunk: zoomStart: %f\n", rank, c.zoomStart);
    printf("Rank %d chunk: zoomEnd: %f\n", rank, c.zoomEnd);
    printf("Rank %d chunk: frames: %d\n", rank, c.frames);
    printf("Rank %d chunk: startFrame: %d\n", rank, c.startFrame);
}

/*
 * We output our files into a directory specified in the parameters.
 * This function sets the program's working directory to be there.
 * If the directory does not exist, function will fail and return 1.
 */
int change_directory(char* dest) {
    int retval = chdir(dest);
    int mpiRank;
    MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
    printf("Rank %d changing to directory %s\n", mpiRank, dest);
    if (retval) {
	perror("Failed to change directory");
    }
    return (retval? 1 : 0);
}

/*
 * Distributes the same directory path to every rank.
 * Master has populated *directory, others have not.
 * Directory path is for image output.
 * Is intended to be called after master has already changed to that directory
 * (to test for validity).
 * Called by master only.
 * Serves the same function as MPI_Bcast except it's for a variable-length string.
 */
int distribute_directory(char *directory) {
    int mpiRank, mpiSize;
    MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);
    for (int i = 1; i < mpiSize; i++) {
	MPI_Send(directory, strlen(directory), MPI_CHAR, i,
		TAG_DIRECTORY_PATH, MPI_COMM_WORLD);
    }
    // no need to change directory here
    free(directory);
    return 0;
}

/*
 * Handles distribution of the work after initial distribution.
 * Expects that we were given at least one frame to start with.
 */
void redist_loop(int mpiSize, chunk_t *allChunk, MPI_Datatype *mpi_chunk_t) {
    /* this bit says "wait on all the ranks, and return
     * the ID of one which sends a TAG_WORK_COMPLETE"
     * two ways to implement this: some variant of MPI_Waitall which waits on any;
     * or have an array of eg ints, one per slave rank, and have 1 == active
     * and IRecv on each of those in turn (we're a full-core process anyway)
     *
     * Even if we do have an MPI_Waitall variant,
     * we need to answer the question "any more slaves working"
     */
    int *slaves = malloc((mpiSize-1)*sizeof(int)); // active slaves
    int i, anyRanksWorking, rankSentRequest, signal;
    anyRanksWorking = 1; // boolean - true for yes, there's a rank left
    MPI_Status status;
    for (i=0; i < mpiSize-1; i++) {
	slaves[i] = 1; // initialise slaves[] to 1
    }
    while (1) { // main loop
	// I expect this line to comprise most of master's run-time
	// Wait for a slave to say that it has completed!
	printf("Master: waiting for a TAG_WORK_COMPLETE\n");
	MPI_Recv(&i, 1, MPI_INT, MPI_ANY_SOURCE, TAG_WORK_COMPLETE,
		MPI_COMM_WORLD, &status); // using i because don't care about value
	rankSentRequest = status.MPI_SOURCE; // a RANK value (from 1)
	printf("Master: received a TAG_WORK_COMPLETE from rank %d\n",
		rankSentRequest);
	/*
	 * This bit handles the request for more work.
	 * Either we have no work left, and the slave is to finalize;
	 * or we break off some or all of the remaining work,
	 * and give it to the slave to process.
	 */
	if (allChunk->frames == 0) { // no work left
	    signal = SIGNAL_FINALIZE;
	    MPI_Send(&signal, 1, MPI_INT, rankSentRequest, TAG_DISTRIBUTE, MPI_COMM_WORLD);
	    printf("Master: sent SIGNAL_FINALIZE to rank %d\n", rankSentRequest);
	    slaves[rankSentRequest-1] = 0; // slaves is an ARRAY value (from 0)
	    // are there any slaves still working?
	    anyRanksWorking = 0;
	    for (i = 0; i < mpiSize-1; i++) {
		if (slaves[i] == 1) {
		    anyRanksWorking = 1; // yes
		    break;
		}
	    }
	    if (anyRanksWorking == 0) {
		return; // this is our exit point
	    }
	    rankSentRequest = 0; // reset
	    continue;
	} else { // work left
	    int signal = SIGNAL_MORE_WORK;
	    MPI_Send(&signal, 1, MPI_INT, rankSentRequest, TAG_DISTRIBUTE, MPI_COMM_WORLD);
	    chunk_t nextChunk;
	    nextChunk = break_off_chunk(allChunk);
	    MPI_Send(&nextChunk, 1, *mpi_chunk_t, rankSentRequest, TAG_CHUNK, MPI_COMM_WORLD);
	    printf("Master: sent new work to rank %d\n", rankSentRequest);
	    rankSentRequest = 0; // reset
	    continue;
	}
    }

    free(slaves); // ahahaha
    return;
}

/*
 * Algorithm!
 * Removes a section of work from allChunk and returns it as a chunk.
 * May empty out allChunk.
 * Breaks off chunks in shallow->deep zoom order.
 * Assumes allChunk contains at least 1 frame.
 */
chunk_t break_off_chunk(chunk_t *allChunk) {
    chunk_t returnChunk;

    // there's room to experiment with implementations
    // right now it'll be a basic "take a tenth out".
    // solves the "1/10=0" thing by adding 1 (takes the greater part).

    // this could be implemented with less memory and fewer lines
    // but consider it a readable solution
    int allFrames = allChunk->frames; // a copy which is not modified
    int framesForNewChunk = allFrames/10+1;
    returnChunk.frames = framesForNewChunk;
    allChunk->frames -= framesForNewChunk;

    double magnification_per_frame = (allChunk->zoomEnd - allChunk->zoomStart)/allFrames;
    returnChunk.zoomStart = allChunk->zoomStart;
    returnChunk.zoomEnd = returnChunk.zoomStart + (magnification_per_frame*framesForNewChunk);
    allChunk->zoomStart = returnChunk.zoomEnd;

    returnChunk.startFrame = allChunk->startFrame;
    allChunk->startFrame += framesForNewChunk;

    returnChunk.resolution = allChunk->resolution;
    returnChunk.maxIters = allChunk->maxIters;

    return returnChunk;
}
