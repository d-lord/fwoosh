#ifndef FRACTAL_H__
#define FRACTAL_H__
#include "fwoosh.h"
#include <math.h>

/* Calculates Mandelbrot parameters based on the current frameNo */
void make_mandelbrot_on_frame(pixel *bmp, int bmpX, int bmpY,
	coords_t *coords,  int frameNo, int totalFrames, int maxIters);
/* renders a mandelbrot frame to the bitmap */
int get_mandelbrot(pixel *bmp, mpfr_t r, mpfr_t i, int bmpX, int bmpY,
	mpfr_t magnification, mpfr_prec_t prec, int maxIters);
/* determine the pixel of a colour based on the number of iterations
 * doesn't expect to be called on 0 iterations, may do bad things if so */
void get_pixel_colour(pixel* p, int n, int max);

/* these functions are for testing png code - simple images */
pixel get_pixel(int, int, int);
unsigned char get_red(int, int, int);
unsigned char get_green(int, int, int);
unsigned char get_blue(int, int, int);

/* uses a palette to figure out the required precision at this zoom level */
mpfr_prec_t get_precision_at_zoom(int zoom);
/* an overload-ish for get_precision_at_zoom for the mpfr_t type */
mpfr_prec_t get_precision_at_magnification(mpfr_t magnification);

#endif
