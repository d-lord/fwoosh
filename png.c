#include "png.h"

/*
 * Write a bitmap to file.
 * Will use the filename "img%06d", where d is frameNo.
 * Does not modify bmp.
 */
void write_png(pixel* bmp, int bmpX, int bmpY, int frameNo) {

    /* A lot of credit goes to
     * http://www.libpng.org/pub/png/libpng-1.2.5-manual.html (section IV) 
     * also http://www.lemoda.net/c/write-png/ */

    FILE *fp;
    char path[1000]; // FIXME - works but dumb (will only run out after an int overflows, though)
    sprintf(path, "img%06d.png", frameNo);

    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;
    png_byte ** row_pointers = NULL;
    int pixel_size = 3; // courtesy pem
    int depth = 8;      // a channel is uint8_t

    int x, y; // loop iterators

    if (!(fp = fopen(path, "wb"))) { // write, binary
	return;
    }

    /* perform a ton of libpng setup */

    /* These calls are straight from the documentation - I can't come up with
     * an alternative but equally sensible way to accomplish this */
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) {
	fclose(fp);
    }

    info_ptr = png_create_info_struct (png_ptr);
    if (!info_ptr) {
	png_destroy_write_struct(&png_ptr, &info_ptr);
	fclose(fp);
    }

    png_set_IHDR (  png_ptr,
		    info_ptr,
		    bmpX,
		    bmpY,
		    depth,
		    PNG_COLOR_TYPE_RGB,
		    PNG_INTERLACE_NONE,
		    PNG_COMPRESSION_TYPE_DEFAULT, // "must be" this
		    PNG_FILTER_TYPE_DEFAULT);

    /* finally build the image */
    // master pointer to a series of rows, ie a grid
    row_pointers = png_malloc(png_ptr, bmpY * sizeof(png_byte*));
    for (y = 0; y < bmpY; y++) {
	png_byte *row = png_malloc(png_ptr, sizeof(uint8_t)*bmpY*pixel_size);
	// pixel_size is a magic number
	row_pointers[y] = row;
	// populate the row
	for (x = 0; x < bmpX; x++) {
	    *row++ = bmp[y*bmpX + x].red;
	    *row++ = bmp[y*bmpX + x].green;
	    *row++ = bmp[y*bmpX + x].blue;
	}
    }

    /* write out */
    png_init_io (png_ptr, fp);
    png_set_rows (png_ptr, info_ptr, row_pointers);
    png_write_png (png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

    /* free up and leave */
    for (y = 0; y < bmpY; y++) {
        png_free (png_ptr, row_pointers[y]);
    }
    png_free (png_ptr, row_pointers);
    fclose(fp);
}
