#ifndef PNG_H__
#define PNG_H__
#include "fwoosh.h"
#include <stdlib.h>
#include <png.h>
#include <string.h>

void write_png(pixel* bmp, int bmpX, int bmpY, int frameNo);
#endif
