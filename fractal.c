#include "fractal.h"

/*
 * Calculates Mandelbrot parameters based on the current frameNo.
 * Basically interpolates from the current parameters to the next.
 * Is a wrapper for get_mandelbrot().
 * Updates current and bmp; will draw current then modify it.
 * start and dest are magnification values (to become powers of two).
 * frameNo is the number of frames through the zoom
 */
void make_mandelbrot_on_frame(pixel *bmp, int bmpX, int bmpY,
	coords_t *coords,  int frameNo, int totalFrames, int maxIters) {
    mpfr_t magnification; // used for increasing magnification factor below
    // magnification = (dest-start)/totalFrames*(frameNo+1)
    mpfr_init2(magnification, coords->bits);
    mpfr_sub(magnification, coords->dest, coords->start, MPFR_RNDD);
    mpfr_div_ui(magnification, magnification, totalFrames, MPFR_RNDD);
    mpfr_mul_ui(magnification, magnification, frameNo+1, MPFR_RNDD);
    mpfr_add(magnification, magnification, coords->start, MPFR_RNDD);

    mpfr_prec_t variable_precision = get_precision_at_magnification(magnification);

    // this is the importantest line of them all
    get_mandelbrot(bmp, coords->r, coords->i, bmpX, bmpY, magnification, variable_precision, maxIters);

    mpfr_clear(magnification);
}

/*
 * Draws a Mandelbrot image with the specified parameters to the bitmap.
 * Doesn't do any memory allocation!
 * Uses the fancy map[y*j + i] addressing
 * "r" is on the x-axis, "i" is on the y-axis
 */
int get_mandelbrot(pixel *bmp, mpfr_t r, mpfr_t i, int bmpX, int bmpY,
	mpfr_t magnification, mpfr_prec_t prec, int maxIters) {
    /* Variable definition and explanation time!
     * rSize and iSize denote the length of this image in that axis.
     * rMin and iMax denote the smallest r-coord and largest i-coord.
     * rStep and iStep denote the size of a single pixel in that axis.
     * p, q, ptemp are all temporary variables,
     * as are psq and qsq - square values. MPFR requires more vars than C.
     *
     * Where possible, the original C code (for doubles) is left in comments 
     * to spare the reader from interpreting assembly-style code unnecessarily.
     */
    mpfr_t rSize, rMin, iSize, iMax, rStep, iStep, rCoord, iCoord, p, q, ptemp, psq, qsq;
    mpfr_inits2(prec, rSize, rMin, iSize, iMax, rStep, iStep, rCoord, iCoord, 
	    p, q, ptemp, psq, qsq, (mpfr_ptr) NULL);
    // rSize = pow(2., -magnification) * BASESIZE;
    // using ptemp here because a) unused and b) don't modify original var
    mpfr_neg(ptemp, magnification, MPFR_RNDD);
    mpfr_exp2(ptemp, ptemp, MPFR_RNDD);
    mpfr_mul_ui(rSize, ptemp, BASESIZE, MPFR_RNDD);

    // iSize = pow(2., -magnification) * BASESIZE;
    // ptemp still contains 2^^-mag
    // with forced-rectangles this could be just set to rSize
    mpfr_mul_ui(iSize, ptemp, BASESIZE, MPFR_RNDD);

    // rMin = r - rSize/2;
    mpfr_div_ui(ptemp, rSize, 2, MPFR_RNDD);
    mpfr_sub(rMin, r, ptemp, MPFR_RNDD);

    // iMax = i + iSize/2; // note min vs max
    mpfr_div_ui(ptemp, iSize, 2, MPFR_RNDD);
    mpfr_add(iMax, i, ptemp, MPFR_RNDD);

    // rStep = rSize/bmp->x;
    mpfr_div_ui(rStep, rSize, bmpX, MPFR_RNDD);
    // iStep = iSize/bmp->y;
    mpfr_div_ui(iStep, iSize, bmpY, MPFR_RNDD);
    //printf("rStep = %Rf, iStep = %Rf\n", rStep, iStep);

    // the pixel's img x-coord (from 0 to bmp->x-1) - 0 is left
    int rScreen;
    // the pixel's img y-coord (from 0 to bmp->y-1) - 0 is top
    int iScreen;
    // the pixel's Mandelbrot coord
    // the current iteration of the pixel (from 0 to ITERS)
    int iteration = 0;

    // nested for-loop, let's make this image!
    for (iScreen = 0; iScreen < bmpY; iScreen++) {
	// top row is the first checked; so the i-coord is that distance from the top
	// iCoord = iMax - iScreen*iStep;
	mpfr_mul_ui(ptemp, iStep, iScreen, MPFR_RNDD);
	mpfr_sub(iCoord, iMax, ptemp, MPFR_RNDD);	
	for (rScreen = 0; rScreen < bmpX; rScreen++) {
	    iteration = 0;
	    // rCoord = rMin + rScreen*rStep;
	    mpfr_mul_ui(ptemp, rStep, rScreen, MPFR_RNDD);
	    mpfr_add(rCoord, rMin, ptemp, MPFR_RNDD);
	    // p = rCoord;
	    mpfr_set(p, rCoord, MPFR_RNDD);
	    // q = iCoord;
	    mpfr_set(q, iCoord, MPFR_RNDD);
	    // this is where the magic happens
#ifdef FULLDEBUG
	    printf("pixel: about to enter while-loop: dumping vars\n");
	    mpfr_printf("iCoord = %Rf\nrCoord = %Rf\n", iCoord, rCoord);
	    mpfr_printf("p (should be rCoord) = %Rf\nq (should be iCoord) = %Rf\n", p, q);
#endif

	    while (iteration < maxIters) {
		mpfr_mul(psq, p, p, MPFR_RNDD);
		mpfr_mul(qsq, q, q, MPFR_RNDD);
		mpfr_add(ptemp, psq, qsq, MPFR_RNDD);
		if (mpfr_cmp_ui(ptemp, 4) >= 0) {
		    break;
		}
		// ptemp = p*p - q*q + rCoord;
		// that is, ptemp = psq - qsq + rCoord
		// at this point: psq is populated, ptemp is useless
		mpfr_sub(ptemp, psq, qsq, MPFR_RNDD);
		mpfr_add(ptemp, ptemp, rCoord, MPFR_RNDD);
		// qsq is no longer valuable (about to be outdated) so we use it
		// q = 2*p*q + iCoord;
		mpfr_mul(qsq, p, q, MPFR_RNDD);
		mpfr_mul_ui(qsq, qsq, 2, MPFR_RNDD);
		mpfr_add(q, qsq, iCoord, MPFR_RNDD);

		// p = ptemp;
		mpfr_set(p, ptemp, MPFR_RNDD);
		iteration++;
	    }
	    if (iteration == maxIters) {
		// hit cap - may be part of the set - yellow
		bmp[iScreen*bmpX + rScreen].red = 255;
		bmp[iScreen*bmpX + rScreen].green = 255;
		bmp[iScreen*bmpX + rScreen].blue = 0;
	    } else {
		// not yellow - should be coloured
		get_pixel_colour(&bmp[iScreen*bmpY + rScreen], iteration, maxIters);
	    }
	}
    }
    mpfr_clears(rSize, rMin, iSize, iMax, rStep, iStep, rCoord, iCoord, p, q, 
	    ptemp, psq, qsq, (mpfr_ptr) NULL);
    return 0;
}

/* determine the pixel of a colour based on the number of iterations 
 * doesn't expect to be called on 0 iterations, may do bad things if so */
void get_pixel_colour(pixel* p, int n, int max) {
    p->red = (n%4)*127;
    p->green = 0;
    p->blue = (n%4)*127;
    return;
}

/* uses a palette to figure out the required precision at this zoom level */
mpfr_prec_t get_precision_at_zoom(int zoom) {
    // TODO: populate this properly
    if (zoom < 25) {
	return 32;
    } else if (zoom < 40) {
	return 48;
    } else if (zoom < 60) {
	return 64;
    } else if (zoom < 120) {
	return 128;
    } else if (zoom < 220) {
	return 256;
    } else {
	return 512; // !!
    }
}

/* an overload-ish for get_precision_at_zoom for the mpfr_t type */
mpfr_prec_t get_precision_at_magnification(mpfr_t magnification) {
    long int mag_rnd = mpfr_get_ui(magnification, MPFR_RNDU);
    if (mag_rnd > INT_MAX) {
	mpfr_fprintf(stderr, "Magnification %Rf exceeds INT_MAX;"
		" this may cause problems\n", magnification);
    }
    return get_precision_at_zoom(mag_rnd);
}

/* below this line is obsolete, but kinda pretty */
pixel get_pixel(int i, int j, int frameNo) {
    pixel ret;
    ret.red = get_red(i, j, frameNo);
    ret.green = get_green(i, j, frameNo);
    ret.blue = get_blue(i, j, frameNo);
    return ret;
}

unsigned char get_red(int i, int j, int frameNo) {
    //float x = (float) i;
    //return ((x/XRES) * 256);
    return i+frameNo % 256;
}

unsigned char get_green(int i, int j, int frameNo) {
    float y = (float) j;
    return (y/YRES) * 256;
    //return j+frameNo % 256;
}

unsigned char get_blue(int i, int j, int frameNo) {
    //return ((i+j) +(frameNo)) %256;
    return 0;
}
