## fwoosh ##
"Fractal: the Movie".
Renders a Mandelbrot zoom as a movie.

Distributed fractal renderer for computing clusters. Parallelised using both OpenMP and MPI for use on the Savanna and Barrine clusters at UQ. Arbitrary precision for deep zooms is implemented using the MPFR and GMP libraries.

COSC3500, 2014, semester 2.
[See an example on Vimeo](https://vimeo.com/105115846)

![Title image](https://bitbucket.org/d-lord/fwoosh/raw/master/fractals/yellow-banding.png)