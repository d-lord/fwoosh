#ifndef SLAVE_H__
#define SLAVE_H__
#include "fwoosh.h"

#define PROGRESS 1	// reports "drawing frameNo = ..." every PROGRESS frames

/*
 * The main drawing loop for a slave process.
 * Returns once the slave has completed its chunk.
 * Uses OpenMP to thread the task.
 */
int slave_draw_loop(chunk_t myChunk, coords_t *coords, int mpiRank, int mpiSize);

/*
 * Gets startup data.
 * Specifically, a signal for whether the job will run or not,
 * and a directory to put images in.
 * Job may not run if the argument parsing fails.
 * Returns either SIGNAL_OK or SIGNAL_KILL, and assigns into directory.
 */
int get_startup_data(char** directory);

/*
 * Called when the current chunk has been fully drawn.
 * Notifies the master rank that this rank has completed its work.
 * Master will send a signal in response, but that's not part of this function:
 * see get_chunk().
 */
void notify_master(void);

/*
 * Called when this rank needs a new chunk of work.
 * This is true both for initial and redistributed work.
 * Master will send a signal, then any work.
 * Either master will send new work, or master will send a "game over" signal.
 * Returns either SIGNAL_MORE_WORK or SIGNAL_FINALIZE.
 * Updates the chunk if that's appropriate.
 */
int get_chunk(chunk_t *myChunk, MPI_Datatype mpi_chunk_t);


#endif
