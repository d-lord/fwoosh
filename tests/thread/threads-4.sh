#PBS -l nodes=1:ppn=1+1:ppn=4
#PBS -lmem=4gb
#PBS -lwalltime=01:00:00
#PBS -A uq-COSC3500
#PBS -q cosc3500
#PBS -N threads-4
#PBS -m ae

# one process for master, others for slaves
export MPI_NUM_PROCS=2
echo "MPI_NUM_PROCS is $MPI_NUM_PROCS"
export OMP_NUM_THREADS=4
echo "OMP_NUM_THREADS is $OMP_NUM_THREADS"

export RES=1024
export ITERS=2000
export STARTZOOM=50
export ENDZOOM=100
export FRAMES=200
export OUTPUT_DIRECTORY=img

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/cosc3500/s4303907/fwoosh/thirdparty/lib

cd $PBS_O_WORKDIR

time mpirun -n $MPI_NUM_PROCS ./fwoosh -r $RES -i $ITERS -s $STARTZOOM -e $ENDZOOM -f $FRAMES -d $OUTPUT_DIRECTORY
